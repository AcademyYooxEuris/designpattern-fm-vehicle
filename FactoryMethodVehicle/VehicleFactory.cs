﻿namespace FactoryMethodVehicle
{
    public class VehicleFactory
    {
        public static IVehicle Build(int numberOfWheels, int cilindrata)
        {
            switch (numberOfWheels)
            {
                case 2:
                    if (cilindrata > 125) return new Motorbike(cilindrata); else return new Scooter(cilindrata);
                case 4:
                    return new Car(cilindrata);
                case 6:
                    return new Truck(cilindrata);
                default: throw new NumberOfWheelsNotSupported();
            }
        }
    }
}
